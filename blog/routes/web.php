<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Post;
use App\User;
use App\Category;
use Illuminate\Support\Facades\Route; 
use App\Http\Controllers\PostController;


Route::get('/', function () {
    return view('index', [
        "title" => "Home",
        "active" => "Home"
    ]);
});

Route::get('/about', function () {
    return view('about', 
        [
            "title" => "About",
            "active" => "About",
            "name" => "Adnan Erlansyah",
            "email" => "adnanerlansyah403@gmail.com"
        ]
    );
});

// Post
// halaman single post
Route::get('/post', 'PostController@index');
Route::get("/posts/{post}", 'PostController@show');

// Category Post
Route::get('/categories', function() {
    return view('categories', [
        'title' => "Post Categories",
        'active' => "Categories",
        'categories' => Category::all()
    ]);
});
Route::get('/categories/{category}', function( Category $category ) {
    return view('blog', [
        'title' => "Post By Category : " . $category->name,
        'active' => "Post By Category : " . $category->name,
        'posts' => $category->posts->load('category', 'author')
        // 'category' => $category->name
    ]);
});

Route::get('/authors/{author}', function(User $author) {

    // dd($user->post);
    return view('blog', [
        'title' => 'Post By Author : ' . $author->name,
        'active' => 'Post By Author : ' . $author->name,
        'posts' => $author->post->load('category', 'author')
    ]);
});
