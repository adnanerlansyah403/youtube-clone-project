@extends('layouts.main')

@section('container')

    <h1>Post Categories</h1>

    <div class="container">
        <div class="row">

            @foreach( $categories as $category )
            <div class="col-md-4 mb-2">
                <a href="/post?category={{ $category->slug }}">
                    <div class="card bg-dark text-white">
                        <img src="https://source.unsplash.com/random/500x400?{{ $category->name }}" class="card-img-top" alt="{{ $category->name }}">
                        <div class="card-img-overlay d-flex justify-content-center align-items-center p-0">
                        <h5 class="card-title text-center flex-fill p-4 fs-3" style="background: rgba(0, 0, 0, .7)">{{ $category->name }}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
            
        </div>
    </div>

    
            

@endsection