@extends('layouts.main')

@section('container')
    <h1 class="mb-3 text-center">{{ $title }}</h1>

    <div class="row justify-content-center mb-3">
        <div class="col-md-6">
            <form action="/post" method="GET">
               
                @if(request('category'))

                    <input type="hidden" name="category" value="{{ request('category') }}">

                @endif

                @if (request('author'))

                    <input type="hidden" name="author" value="{{ request('author') }}">

                @endif

                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search..."  name="search" value="{{ request('search') }}">
                    <div class="input-group-append">
                      <button class="btn btn-danger" type="submit" id="button-addon2">Search</button>
                    </div>
                  </div>
            </form>
        </div>
    </div>

    @if($posts->count())
    
        <div class="card mb-3">
            <img src="https://source.unsplash.com/random/1200x400?{{ $posts[0]->category->name }}" class="card-img-top" alt="{{ $posts[0]->category->name }}">
            <div class="card-body text-center">
                <h3 class="card-title"><a href="/posts/{{ $posts[0]->slug }}" class="text-decoration-none text-dark">{{ $posts[0]->title }}</a></h3>
                <p>
                    <small>
                    By. <a href="/post?author={{ $posts[0]->author->username }}">{{ $posts[0]->author->name }}</a> In <a href="/post?category={{ $posts[0]->category->slug }}" class="text-decoration-none">{{ $posts[0]->category->name }}</a> {{ $posts[0]->created_at->diffForHumans() }}
                    </small>
                </p>
                <p class="card-text">{{ $posts[0]->excerpt }}</p>

                <a href="/posts/{{ $posts[0]->slug }}" class="text-decoration-none btn btn-primary">Read Mores</a>

            </div>
        </div>

    <div class="container">
        <div class="row">

            @foreach( $posts->skip(1) as $key => $post ) 
                <div class="col-sm-6 col-md-4 mb-3">
                    <div class="card">
                        <div class="position-absolute py-2 px-3 " style="background: rgba(0, 0, 0, .7)"><a href="/post?category={{ $post->category->slug }}" class="text-light">{{ $post->category->name }}</a></div>
                        <img src="https://source.unsplash.com/random/600x400?{{ $post->category->name }}" class="card-img-top" alt="$post->category->name">
                        <div class="card-body">
                        <h5 class="card-title">{{ $post->title }}</h5>
                        <p>
                            <small>
                            By. <a href="/post?author={{ $post->author->username }}">{{ $post->author->name }}</a>{{ $post->created_at->diffForHumans() }}
                            </small>
                        </p>
                        <p class="card-text">{{ $post->excerpt }}</p>
                        <a href="/posts/{{ $post->slug }}" class="btn btn-primary">Read More</a>
                        </div>
                    </div>
                </div>
            @endforeach
            
        </div>
    </div>

    @else

        <p class="text-center fs-4">No Post Found.</p>

    @endif

    <div class="container d-flex justify-content-center">
    {{ $posts->links() }}
    </div>

@endsection