<nav class="navbar navbar-expand-lg navbar-dark bg-danger">
    <div class="container">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item {{ ($active === 'Home') ? 'active' : ''  }}">
              <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item {{ ($active === 'About') ? 'active' : ''  }}">
              <a class="nav-link" href="/about">About</a>
            </li>
            <li class="nav-item {{ ($active === 'Posts') ? 'active' : ''  }}">
              <a class="nav-link" href="/post">Blog</a>
            </li>
            <li class="nav-item {{ ($active === 'Categories') ? 'active' : ''  }}">
              <a class="nav-link" href="/categories">Categories</a>
            </li>
            <li class="nav-item {{ ($active === 'Single Posts') ? 'active' : ''  }}">
            </li>
          </ul>
        </div>
    </div>
</nav>