@extends("layouts.main")

@section("container")

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>{{ $post->title }}</h2>
            <p>By. <a href="/post?author={{ $post->author->username }}" class="text-decoration-none">{{ $post->author->name }}</a> In <a href="/post?category={{ $post->category->slug }}">{{ $post->category->name }}</a></p>

            <img src="https://source.unsplash.com/random/600x400?{{ $post->category->name }}" class="card-img-top" alt="$post->category->name" class="img-fluid">

            <article class="my-3 fs-5">
                {!! $post->body !!}
            </article>
    
            <a href="/post" class="mt-3 d-block">Back to Posts</a>
        </div>
    </div>
</div>

@endsection