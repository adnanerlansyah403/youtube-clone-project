<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Contracts\Service\Attribute\Required;

class PostController extends Controller
{
    
    //
    public function index()
    { 

        $title = '';

        if(request('category')) {
            $category = Category::firstWhere('slug', request('category'));
            $title = ' in ' . $category->name;
        }

        if(request('author')) {
            $author = User::firstWhere('username', request('author'));
            $title = ' by ' . $author->name;
        }


        return view('blog', [
            "title" => "All Posts" . $title,
            "active" => "Posts",
            // "posts" => Post::all()
            "posts" => Post::latest()->filter(request(['search', 'category', 'author']))->paginate(7)->withQueryString()
        ]);
    }

    public function show(Post $post) {
        
        // $slug = DB::table('posts')->where('slug', $request->slug)->get()->first();

        return view("post", [
            'title' => 'Single Post',
            'active' => 'Single Post',
            'post' => $post
        ]);
    }
}
