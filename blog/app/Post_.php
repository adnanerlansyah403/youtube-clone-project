<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post
{
    private static $blog_posts = [
        [
            "title" => "Judul Post 1",
            "slug" => "judul-post-1",
            "author" => "Adnan Erlansyah",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum porro asperiores amet pariatur veniam molestiae laboriosam a doloremque quas aliquid voluptatum nam cupiditate iste, ab ipsam dolor temporibus quasi quae eveniet illo vitae culpa praesentium. Nulla esse magni nemo dignissimos provident aliquid ducimus error ipsam cum voluptatum? Sint, quidem. Esse pariatur obcaecati rem. Et, dicta! Consequatur, tempore optio. Eveniet, nisi?",
        ],
        [
            "title" => "Judul Post 2",
            "slug" => "judul-post-2",
            "author" => "Bayu Aji",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum porro asperiores amet pariatur veniam molestiae laboriosam a doloremque quas aliquid voluptatum nam cupiditate iste, ab ipsam dolor temporibus quasi quae eveniet illo vitae culpa praesentium. Nulla esse magni nemo dignissimos provident aliquid ducimus error ipsam cum voluptatum? Sint, quidem. Esse pariatur obcaecati rem. Et, dicta! Consequatur, tempore optio. Eveniet, nisi?",
        ],
        [
            "title" => "Judul Post 4",
            "slug" => "judul-post-4",
            "author" => "Ilyas Aditya",
            "content" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum porro asperiores amet pariatur veniam molestiae laboriosam a doloremque quas aliquid voluptatum nam cupiditate iste, ab ipsam dolor temporibus quasi quae eveniet illo vitae culpa praesentium. Nulla esse magni nemo dignissimos provident aliquid ducimus error ipsam cum voluptatum? Sint, quidem. Esse pariatur obcaecati rem. Et, dicta! Consequatur, tempore optio. Eveniet, nisi?",
        ]
    ];

    public static function all() {
        return collect(self::$blog_posts);
    }

    public static function find($slug) {
        // $posts = self::$blog_posts;
        $post = static::all();

        // $post = [];

        // foreach($posts as $p) {
        //     if($p["slug"] === $slug) {
        //         $post = $p;
        //     }
        // }

        return $post->firstWhere("slug", $slug);
    }
}
