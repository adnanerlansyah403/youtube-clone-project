<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        //
        'title' => $this->faker->sentence(mt_rand(2, 8)),
        'slug' => $this->faker->slug(),
        'excerpt' => $this->faker->paragraph(),
        'body' => collect($this->faker->paragraphs(mt_rand(5, 10)))
                    ->map(fn($p) => "<p>$p</p>")
                    ->implode(''),
        'user_id' => mt_rand(1,3),
        'category_id' => mt_rand(1,2)

    ];
});
