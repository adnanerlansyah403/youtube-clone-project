<?php

use App\Post;
use App\User;
use App\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


        // User::create([
        //     'name' => 'Adnan Erlansyah',
        //     'email' => 'adnanerlansyah403@gmail.com',
        //     'password' => bcrypt('12345')
        // ]); 

        // User::create([
        //     'name' => 'Bayu Aji',
        //     'email' => 'bayuaji23@gmail.com',
        //     'password' => bcrypt('12345')
        // ]); 

        $this->call([
                UserSeeder::class,
                PostSeeder::class
            ]
        );

        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);

        Category::create([
            'name' => 'Web Design',
            'slug' => 'web-design'
        ]);

        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero at voluptatum, nam molestias ipsum voluptate.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis dignissimos dicta similique exercitationem voluptatem ex quaerat aperiam. Esse cum, labore optio veritatis officiis modi ipsum consectetur quasi. Architecto, saepe doloribus.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Judul Kedua',
        //     'slug' => 'judul-ke-dua',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero at voluptatum, nam molestias ipsum voluptate.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis dignissimos dicta similique exercitationem voluptatem ex quaerat aperiam. Esse cum, labore optio veritatis officiis modi ipsum consectetur quasi. Architecto, saepe doloribus.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Judul Ketiga',
        //     'slug' => 'judul-ketiga',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero at voluptatum, nam molestias ipsum voluptate.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis dignissimos dicta similique exercitationem voluptatem ex quaerat aperiam. Esse cum, labore optio veritatis officiis modi ipsum consectetur quasi. Architecto, saepe doloribus.',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);

        // Post::create([
        //     'title' => 'Judul Keempat',
        //     'slug' => 'judul-keempat',
        //     'excerpt' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Libero at voluptatum, nam molestias ipsum voluptate.',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis dignissimos dicta similique exercitationem voluptatem ex quaerat aperiam. Esse cum, labore optio veritatis officiis modi ipsum consectetur quasi. Architecto, saepe doloribus.',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
    }
}
