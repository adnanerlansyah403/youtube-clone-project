<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    private static $countView = 1;

    public function index() {
        return view("home", [
            "title" => "Home",
            "views" => self::$countView
        ]);
    }
}
