<div class="d-flex gap-2">
    {{-- Do your work, then step back. --}}
    {{-- <button wire:click="increment">+</button> --}}
    {{-- <h1>{{ $count }}</h1> --}}
    {{-- <button wire:click="decrement">-</button> --}}

    <h1>{{ $count }}</h1>

    <script>
        var video = document.getElementById("my-video");
      
          video.addEventListener("timeupdate", function() {
            let vid = videojs("my-video");
            let currentTimeVideo = video.currentTime;
            var minutes = Math.floor(currentTimeVideo / 60);   
            var seconds = Math.floor(currentTimeVideo - minutes * 60)
            var x = minutes < 10 ? "0" + minutes : minutes;
            var y = seconds < 10 ? "0" + seconds : seconds;
      
            // console.log(x + " : " + y);
      
            if(y > 3) {
              // console.log("hello");
              Livewire.emit('countViews')
            }
          }); 
      </script>

</div>
