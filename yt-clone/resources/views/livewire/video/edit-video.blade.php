<div @if($video->processing_percentage < 100) wire:poll @endif>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

        <div class="row">
            <div class="col-md-4">
                <img src="{{ asset($this->video->thumbnail) }}" class="img-thumbnail" alt="">
            </div>
            <div class="col-md-8">
                <p>processing ({{ $this->video->processing_percentage }})</p>
            </div>
        </div>


        <form wire:submit.prevent="update">

            @if ($video->image)
                <img src="{{ asset('images/' . $video->image) }}" alt="">
            @endif

            @error('video.title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" wire:model="video.title">
            </div>

            @error('video.description')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            <div class="form-group">
                <label for="description">Description</label>
                <textarea cols="30" rows="4" class="form-control" wire:model="video.description"></textarea>
            </div>

            @error('video.visibillity')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror

            <div class="form-group">
                <label for="visibillity">Visibillity</label>
                <select wire:model="video.visibillity" class="form-control">
                    <option value="private">private</option>
                    <option value="public">public</option>
                    <option value="unlisted">unlisted</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

        
            @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            
        </form>
            </div>
        </div>
    </div>
</div>
