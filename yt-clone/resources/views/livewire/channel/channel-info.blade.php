<div class="my-4">
    <div class="d-flex align-items-center justify-content-between">

        <div class="d-flex align-items-center">
            <img src="{{ asset('/images/' . $channel->image) }}" class="rounded-circle img-fluid" alt="channel-image" width="50px" height="50px">
            <div class="ml-2 mt-4">
                <h4>{{ $channel->name }}</h4>
                <p class="gray-text text-sm">{{ $channel->subscribers() }} subscribers</p>
            </div>
        </div>

        <div>
            <button wire:click.prevent="toggle" class="btn btn-lg btn-secondary text-uppercase {{ ($userSubscribed) ? 'sub-btn-active' : '' }}">
                {{ ($userSubscribed) ? 'Subscribe' : 'Subscribed' }}
            </button>
        </div>

    </div>
</div>
