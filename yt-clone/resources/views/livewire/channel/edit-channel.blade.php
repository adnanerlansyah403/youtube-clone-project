<div>
    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day. --}}

    <form wire:submit.prevent="update">

        @if ($channel->image)
            <img src="{{ asset('images/' . $channel->image) }}" alt="">
        @endif

        @error('channel.name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" wire:model="channel.name">
        </div>

        <div class="form-group">
            <label for="slug">Slug</label>
            <input type="text" class="form-control" wire:model="channel.slug">
        </div>

        @error('channel.description')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
        <div class="form-group">
            <label for="description">Description</label>
            <textarea cols="30" rows="4" class="form-control" wire:model="channel.description"></textarea>
        </div>

        @error('image')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
        <div class="form-group">
            <input type="file" wire:model="image" class="form-control">
        </div>

        <div class="form-group">
            @if ($image)
                Photo Preview:
                <img src="{{ $image->temporaryUrl() }}" class="img-fluid">
            @endif
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

       
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        
    </form>

</div>
