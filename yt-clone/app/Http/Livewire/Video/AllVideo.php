<?php

namespace App\Http\Livewire\Video;

use App\Models\Video;
use App\Models\Channel;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AllVideo extends Component
{

    use WithPagination;
    use AuthorizesRequests;

    protected $paginationTheme = 'bootstrap';

    public $channel;

    // public $videos;

    // public function mount()
    // {
    //     $this->videos = auth()->user()->channel->videos;
    // }

    public function mount(Channel $channel)
    {
        $this->channel = $channel;
    }

    public function render()
    {
        return view('livewire.video.all-video')
        // ->with('videos', auth()->user()->channel->videos()->paginate(1))
        ->with('videos', $this->channel->videos()->paginate(1))
        ->extends('layouts.app');
    }


    public function delete(Video $video)
    {

        // check if user is allowed to delete the video

        $this->authorize('delete', $video);

        // dd($video);
        // delete the video
        $deleted = Storage::disk('videos')->deleteDirectory($video->uid);

        if($deleted) {
            $video->delete();
        }

        return back();
    }

}

