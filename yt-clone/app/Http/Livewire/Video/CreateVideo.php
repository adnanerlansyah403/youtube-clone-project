<?php

namespace App\Http\Livewire\Video;

use App\Models\Video;
use App\Models\Channel;
use Livewire\Component;
use Livewire\WithFileUploads;
use App\Jobs\ConvertVideoForStreaming;
use App\Jobs\CreateThumbnailFromVideo;

class CreateVideo extends Component
{


    use WithFileUploads;

    public Channel $channel;
    public Video $video;
    public $videoFile;

    protected $guarded = ['id'];

    protected $rules = [
        'videoFile' => 'required|mimes:mp4|max:1228800'
    ];

    public function mount(Channel $channel)
    {
        $this->channel = $channel;
    }

    public function render()
    {
        return view('livewire.video.create-video')
        ->extends('layouts.app');
    }

    public function fileCompleted()
    {
        // dd('file completed');

        // validation
        $this->validate();

        // save the file
        $path = $this->videoFile->store('videos-temp');

        // create video record in db
        $this->video = $this->channel->videos()->create([
            'title' => 'untitle',
            'description' => 'none',
            'uid' => uniqid(true),
            'visibillitiy' => 'private',
            'path' => explode('/', $path)[1]
        ]);

        // Dispatch jobs

        CreateThumbnailFromVideo::dispatch($this->video);
        ConvertVideoForStreaming::dispatch($this->video);


        // redirect to edit route
        return redirect()->route('video.edit', [
            'channel' => $this->channel,
            'video' => $this->video,
        ]);

    }

    // public function upload()
    // {
    //     $this->validate([
    //         'videoFile' => 'required|mimes:mp4|max:102400',
    //     ]);
    // }
}
