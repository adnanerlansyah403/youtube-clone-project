<?php

namespace App\Console\Commands;

// use FFMpeg\FFMpeg;
use FFMpeg\Format\Video\X264;
use Illuminate\Console\Command;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;

class VideoEncode extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video-encode:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test Video Encoding';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $low = (new X264('aac'))->setKiloBitrate(500);
        $high = (new X264('aac'))->setKiloBitrate(1000);

        FFMpeg::fromDisk('videos-temp')
        ->open('sample.mp4')
        ->exportForHLS()
        ->addFormat($low, function($filters) {
            $filters->resize(648, 483);
        })
        ->addFormat($high, function($filters) {
            $filters->resize(1200, 720);
        })
        ->onProgress(function($progress) {
            $this->info("Progress = {$progress}%");
        })
        ->toDisk('videos-temp')
        ->save('/tests/file.m3u8');

        // FFMpeg::fromDisk('videos-temp')
        // ->open('sample.mp4')
        // ->export()
        // ->inFormat(new \FFMpeg\Format\Video\X264)
        // ->resize(640, 480)
        // ->save('sample_resized.mp4');
    }
}
